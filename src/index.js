/**
 * Created by parra on 11/1/17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import RegistrationPhaseOne from './components/RegistrationPhaseOne'
import InformationSent from './components/InformationSent'

import 'bootstrap/dist/css/bootstrap.css';
import './style/index.css'


ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path='/information-sent' component={InformationSent}></Route>
      <Route path='/' component={RegistrationPhaseOne}></Route>
    </Switch>
  </BrowserRouter>,
    document.getElementById('root'));
