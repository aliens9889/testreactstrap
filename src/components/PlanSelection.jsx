import React, { Component } from 'react';
import classNames from 'classnames';

class PlanSelection extends Component {

    render() {
        const { plans, planSelected } = this.props;

        const renderPlans = () => {
            if (plans.length === 0) {
                return null;
            }
            
            const planComponents = Object.keys(plans[0]).map((months, key) => {
                const description = plans[0][months];

                const planClasses = classNames({'list-group-item-action planPressed': months === this.props.planSelected}, {'list-group-item-action': months !== this.props.planSelected});
                return (
                    <div key={key} className="col">
                        <div className="grid-title">{months} months</div>
                        <br/>
                        <div className="list-group border">
                            {/*<a className="list-group-item-action">*/}
                            <a 
                            // className={ planSelected ? 'list-group-item-action planPressed' : 'list-group-item-action'}
                            className={planClasses}
                            onClick={() => this.props.onClickPlanSelected(months)}>
                                <p className="mt-3">{description.uvaAmount} UVAS</p>
                                <hr className="hr-plan" />
                                <p>${description.pesosAmount}</p>
                                <hr className="hr-plan" />
                                <p>${description.minimunNetIncome}</p>
                            </a>
                        </div>
                    </div>
                );
            });

            return planComponents;
        }

        return (
            <div className="mt-5 mb-3">
                <div>
                    <h6 className="plan-title">Plans</h6>
                    <p className="plan-subtitle">Choose your plan</p>
                </div>

                <div className="container">
                    <div className="row justify-content-between">
                        <div className="col">
                            <div className="grid-title">Term</div>
                            <br/>
                            <div className="grid-title">Weekly payment</div>
                            <br/>
                            <div className="grid-title">Minimum net income</div>
                        </div>
                        { renderPlans() }
                    </div>
                </div>
            </div>

        )
    }


}

export default PlanSelection