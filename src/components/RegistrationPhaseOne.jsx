import React, {Component} from 'react';
import {Alert, Button, Form, FormGroup, Input} from 'reactstrap';
import {withRouter} from "react-router-dom";
import axios from "axios";
import _ from 'underscore';
import {validateEmail, validatePhone} from '../utils/validators'

import PlanSelection from './PlanSelection'
import LoanAssetCalculation from './LoanAssetCalculation'

import logo from '../images/LOGO_Aracar-01.svg'

const CUIT_VALIDATOR_URL = 'http://aracar.vascarsolutions.com/backEnd/api/loan/cuitValidator/';
const PLANS_CALCULATOR_URL = 'http://aracar.vascarsolutions.com/backEnd/api/loan/plansCalculator/';

const CREATE_LOAN = "http://aracar.vascarsolutions.com/backEnd/api/loan";

const GET_ALL_LOANS_TYPES = 'http://aracar.vascarsolutions.com/backEnd/api/loanType/';

const debounce = _.debounce;
const findWhere = _.findWhere;
const TYPING_WAIT = 500;

class RegistrationPhaseOne extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            cuit: '',
            cuitIsValid: null,
            isPhoneValid: null,
            isEmailValid: null,
            mobilePhone: '',
            email: '',
            loanValue: '',
            assetValue: '',
            assetAge: [],
            assetAgeId: null,
            loadingCuitRequest: false,
            loadingPlansRequest: false,
            plans: [],
            validForm: false,
            planSelected: '',
            minimumNetIncome: '',
            weeklyPayment: '',
            msgFirstName: null,
            msgLastName: null,
            msgCuitError: 'The cuit input must have 11 digits',
            msgPhoneError: 'The phone number format invalid',
            msgEmailError: 'The email format is not correct',
            msgLoanError: null,
            msgAssetError: null,
            creditPlans: [],
            loanId: null,
            disabledCalculation: false,
            loadingApplyRequest: false
        };

        this.validateCuit = debounce(this.validateCuit.bind(this), TYPING_WAIT);
        this.validateMobilePhone = debounce(this.validateMobilePhone.bind(this), TYPING_WAIT);
        this.validateEmailRegistration = debounce(this.validateEmailRegistration.bind(this), TYPING_WAIT);
        this.checkPlans = debounce(this.checkPlans.bind(this), TYPING_WAIT);
        this.onChangeCuit = this.onChangeCuit.bind(this);
        this.onChangeMobilePhone = this.onChangeMobilePhone.bind(this);
        this.onChangeInput = this.onChangeInput.bind(this);
        this.onChangeLoanValue = this.onChangeLoanValue.bind(this);
        this.onChangeAssetValue = this.onChangeAssetValue.bind(this);
        this.onChangeAssetAge = this.onChangeAssetAge.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onClickPlanSelected = this.onClickPlanSelected.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.checkFirstName = this.checkFirstName.bind(this);
        this.checkLastName = this.checkLastName.bind(this);
        this.checkCuit = this.checkCuit.bind(this);
        this.checkEmail = this.checkEmail.bind(this);
        this.checkPhoneMobile = this.checkPhoneMobile.bind(this);
        this.sendingToInformationSent = this.sendingToInformationSent.bind(this);
        this.showCreditPlans = this.showCreditPlans.bind(this);
        this.onChangeCreditPlans = this.onChangeCreditPlans.bind(this);
    }

    componentDidMount() {

        axios.get(GET_ALL_LOANS_TYPES)
            .then((response) => {
                this.setState({creditPlans: response.data.data});
            })
            .catch((error) => {
                this.setState({creditPlans: []});
            })
    }

    onChangeCreditPlans(e) {
        e.preventDefault();


        if (+e.target.value === -1) return;

        const assetAge = findWhere(this.state.creditPlans, {'loan_types_log_id': +e.target.value}).assetAge;
        const assetAgePlan = assetAge ? assetAge.split(',') : [];
        this.setState({loanId: e.target.value, assetAge: assetAgePlan});

        if (+e.target.value !== -1) {
            this.setState({disabledCalculation: true});
        } else {
            this.setState({disabledCalculation: false});
        }
        console.log(assetAgePlan);

    }

    onChangeLoanValue(e) {
        this.setState({loanValue: e.target.value});
        this.checkPlans();
    }

    onChangeAssetValue(e) {
        this.setState({assetValue: e.target.value});
        this.checkPlans();
    }

    onChangeAssetAge(e) {
        this.setState({assetAgeId: e.target.value});
        this.checkPlans();
    }

    checkPlans() {
        const {loanValue, assetValue, assetAgeId, loanId} = this.state;
        if (loanValue && assetValue && assetAgeId && loanId) {
            this.setState({loadingPlansRequest: true});

            console.log(loanValue, assetValue, assetAgeId, loanId);

            axios.post(PLANS_CALCULATOR_URL, {
                loanValue,
                assetValue,
                assetAge: assetAgeId,
                loan_types_log_id: loanId
            }).then((response) => {
                this.setState({plans: response.data.data, loadingPlansRequest: false});
            }).catch((error) => {
                this.setState({plans: [], loadingPlansRequest: false});
            });
        } else {
            this.setState({plans: []});
        }
    }

    showCreditPlans(e) {
        e.preventDefault();
        axios.get(GET_ALL_LOANS_TYPES)
            .then((response) => {
                console.log(response.data);
            })
    }

    onClickPlanSelected(months) {
        const plan = this.state.plans[0][months];
        const {minimunNetIncome, pesosAmount, uvaAmount, weeklyPayment} = plan;
        this.setState({
            planSelected: months,
            weeklyPayment,
            pesosAmount,
            minimunNetIncome,
            uvaAmount
        });
    }

    onChangeCuit(e) {
        if (e.target.value) {
            this.validateCuit(e.target.value);
        } else {
            this.setState({cuitIsValid: null});
        }

        this.setState({cuit: e.target.value});
    }

    onChangeMobilePhone(e) {
        const {value} = e.target;
        this.setState({mobilePhone: value})
        if (value) {
            this.validateMobilePhone(value)
        }
    }

    onChangeEmail(e) {
        const {value} = e.target;
        this.setState({email: value})
        if (value) {
            this.validateEmailRegistration(value)
        }
    }

    validateEmailRegistration(value) {

        const isEmailValid = this.state.isEmailValid;

        this.setState({isEmailValid: validateEmail(value)})

        if (isEmailValid) {
            this.checkEmail();
        }


    }

    validateMobilePhone(value) {
        const isPhoneValid = this.state.isPhoneValid;

        this.setState({isPhoneValid: validatePhone(value)})

        if (isPhoneValid) {
            this.checkPhoneMobile();
        }

    }

    onChangeInput(field, value, fn) {
        fn = fn ? fn : () => {
        };
        this.setState({
            [field]: value
        }, fn);

        if (field === 'firstName') {
            this.checkFirstName(value);
        } else if (field === 'lastName') {
            this.checkLastName(value);
        }


    }

    validateCuit(value) {
        this.setState({loadingCuitRequest: true});

        axios.post(CUIT_VALIDATOR_URL, {
            cuit: value
        }).then((response) => {
            this.setState({cuitIsValid: response.data.message, loadingCuitRequest: false});
        }).catch((error) => {
            this.setState({cuitIsValid: false, loadingCuitRequest: false});

        });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({loadingApplyRequest: true});
        const {
            firstName,
            lastName,
            cuit,
            mobilePhone,
            email,
            loanValue,
            assetValue,
            assetAgeId,
            minimunNetIncome,
            uvaAmount,
            weeklyPayment,
            pesosAmount,
            loanId,
            loadingApplyRequest
        } = this.state;
        console.log('state', this.state)
        console.log(firstName, lastName, cuit, mobilePhone, email, loanValue, assetValue, assetAgeId, minimunNetIncome, uvaAmount, pesosAmount, weeklyPayment, loanId);
        axios.post(CREATE_LOAN, {
            firstName,
            lastName,
            cuit,
            mobilePhone,
            email,
            loanValue,
            assetValue,
            assetAge: +assetAgeId,
            minimunNetIncome,
            weeklyPaymentUvas: uvaAmount,
            weeklyPaymentPesos: pesosAmount,
            term: weeklyPayment,
            loan_types_log_id: loanId
        })
            .then((response) => {
                console.log(response);
                // this.setState({loadingApplyRequest: true});
                this.sendingToInformationSent();
            })
            .catch((error) => {
                console.error(error);
                // this.sendingToInformationSent();
            })
    }

    checkCuit() {
        const cuitIsValid = this.state.cuitIsValid;
        const mess = this.state.msgCuitError;

        if (cuitIsValid || typeof cuitIsValid !== 'boolean') {
            return null;
        }

        return (
            // <h1 className="left-subtitle">CUIT Invalid</h1>
            <h5 className="left-subtitle">{mess}</h5>
        );
    }

    checkEmail() {
        const isEmailValid = this.state.isEmailValid;
        const msgEmailError = this.state.msgEmailError;

        if (isEmailValid || typeof isEmailValid !== 'boolean') {
            return null;
        }

        return (
            <h5 className="left-subtitle">{msgEmailError}</h5>
        );
    }

    checkPhoneMobile() {
        const isPhoneValid = this.state.isPhoneValid;
        const msgPhoneError = this.state.msgPhoneError;

        if (isPhoneValid || typeof isPhoneValid !== 'boolean') {
            return null;
        }

        return (
            <h5 className="left-subtitle">{msgPhoneError}</h5>
        );
    }

    checkFirstName() {
        const firstName = this.state.firstName;
        const msgFirstName = this.state.msgFirstName;

        if (firstName !== '') {
            return null;
        }

        return (
            <h5 className="left-subtitle">Firstname is required</h5>
        );
    }

    checkLastName() {
        const lastName = this.state.lastName;
        const msgLastName = this.state.msgLastName;

        if (lastName !== '') {
            return null;
        }

        return (
            <h5 className="left-subtitle">Lastname is required</h5>
        );
    }

    checkLoanValue() {
        const loanValue = this.state.loanValue;
        const msgLoanError = this.state.msgLoanError;

        if (loanValue !== '') {
            return null;
        }

        return (

            <h5 className="left-subtitle">{msgLoanError}</h5>
        );
    }

    sendingToInformationSent(e) {
        this.setState({loadingApplyRequest: false});
        this.props.history.push('/information-sent');

    }

    render() {
        const {
            firstName, lastName,
            mobilePhone, email,
            cuit, cuitIsValid,
            loadingCuitRequest, loadingPlansRequest,
            validForm, plans,
            planSelected, isPhoneValid, isEmailValid,
            creditPlans,
            assetAge, disabledCalculation,
            loadingApplyRequest
        } = this.state;

        console.log(this.state.loadingApplyRequest);


        return (
            <div className="container mt-5 mb-5">

                <div className="row">
                    <div className="col-md-5 col-sm-12 left-column col-xs-12" align="center">
                        <div className="mt-lg-5">
                            <h2 className="col-10 offset-col-2 mt-4 left-title">Welcome!</h2>
                        </div>
                        <div className="mt-lg-5">
                            <h1 className="left-subtitle">Applicant Information</h1>
                        </div>
                        <br />

                        <div>
                            {this.checkFirstName()}
                        </div>
                        <div>
                            {this.checkLastName()}
                        </div>
                        <div>
                            {this.checkCuit()}
                        </div>

                        <div>
                            {this.checkPhoneMobile()}
                        </div>

                        <div>
                            {this.checkEmail()}
                        </div>

                        <div>
                            {this.checkLoanValue()}
                        </div>

                    </div>

                    <div className="col-md-7 right-column col-sm-12 col-xs-12" align="center">

                        <div className="text-center img-aracar mt-5">
                            <img src={logo} className="rounded mx-auto d-block" alt="..."/>
                        </div>

                        <Form className="col-11 offest-col-1 mt-5">
                            {(() => {
                                if (cuitIsValid || typeof cuitIsValid !== 'boolean') {
                                    return null;
                                }

                                return <Alert color="danger">CUIT Invalid</Alert>;
                            })()}
                            <FormGroup>
                                <div className="form-row">
                                    <div className="form-group col-xs-12 col-md-6 col-sm-6">
                                        <Input
                                            type="text"
                                            className="form-control"
                                            id="firstName"
                                            placeholder="Individual Borrower Name"
                                            onChange={e => this.onChangeInput('firstName', e.target.value)}
                                        />
                                    </div>
                                    <div className="form-group col-xs-12 col-md-6 col-sm-6">
                                        <Input
                                            type="text"
                                            className="form-control"
                                            id="lastName"
                                            placeholder="Individual Borrower Last Name"
                                            onChange={e => this.onChangeInput('lastName', e.target.value)}
                                        />
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-xs-12 col-md-6 col-sm-6">
                                        <Input
                                            type="text"
                                            className={!loadingCuitRequest ? 'form-control' : 'form-control cuitSpinner' }
                                            id="cuit"
                                            placeholder="CUIT"
                                            onChange={this.onChangeCuit}/>
                                    </div>
                                    <div className="form-group col-xs-12  col-md-6 col-sm-6">
                                        <Input
                                            type="tel"
                                            className="form-control"
                                            id="mobilePhone"
                                            placeholder="Cell Phone"
                                            onChange={this.onChangeMobilePhone}
                                        />
                                    </div>
                                </div>

                                <div className="form-group">
                                    <Input
                                        type="email"
                                        className="form-control"
                                        id="email"
                                        placeholder="Email"
                                        onChange={this.onChangeEmail}
                                    />
                                </div>

                                <LoanAssetCalculation
                                    onChangeLoanValue={this.onChangeLoanValue}
                                    onChangeAssetValue={this.onChangeAssetValue}
                                    onChangeAssetAge={this.onChangeAssetAge}
                                    loadingPlansRequest={loadingPlansRequest}
                                    creditPlans={creditPlans}
                                    onChangeCreditPlans={this.onChangeCreditPlans}
                                    assetAge={assetAge}
                                    disabledCalculation={disabledCalculation}
                                />
                                {
                                    (() => {
                                        if (plans.length === 0) {
                                            return;
                                            // <Alert color="warning">You must enter loan, asset and asset age values to calculate your plans</Alert>
                                        }

                                        return <PlanSelection
                                            plans={plans}
                                            onClickPlanSelected={this.onClickPlanSelected}
                                            planSelected={planSelected}/>;
                                    })()
                                }
                                <br />
                                <Button
                                    type="submit"
                                    color="primary"
                                    disabled={!planSelected}
                                    onClick={this.handleSubmit}>
                                    APPLY</Button>
                                {
                                    loadingApplyRequest && <div className="col-md-12 spinnerLoanAsset">
                                        <i className="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i>
                                    </div>
                                }
                                <div className="spaceBottom"></div>
                            </FormGroup>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}


export default withRouter(RegistrationPhaseOne)


