import React, { Component } from 'react'
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Input} from 'reactstrap';


class LoanAssetCalculation extends Component {

    constructor(props) {
        super(props)
        this.state = {
          assetAges: [0,1,2,3,4,5]
        };
    }

    render () {
        const { assetAges } = this.state; 
        const { loadingPlansRequest, creditPlans, onChangeCreditPlans, assetAge, disabledCalculation } = this.props;
        return (
            <div>
                <div className="form-row">
                <div className="form-group col-md-12 credit-selectetor">
                        <Input 
                            type="select"
                            className="selector"
                            onChange={onChangeCreditPlans}
                            defaultValue='-1'
                            >
                             <option value={-1}>Select Credit Plan</option>
                           {
                               creditPlans.map((plan) => {
                                return <option value={plan.loan_types_log_id} key={plan.loan_types_log_id}>{plan.name}</option>
                               })
                           }
                            
                        </Input>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-4 col-xs-12">
                        <Input 
                            type="text" 
                            className="form-control"
                            id="loanValue"
                            placeholder="Loan Value"
                            onChange={this.props.onChangeLoanValue} 
                            disabled={!this.props.disabledCalculation}
                            />
                    </div>
                    <div className="form-group col-md-4 col-xs-12">
                        <Input 
                            type="text" 
                            className="form-control" 
                            id="assetValue"
                            placeholder="Asset Value"
                            onChange={this.props.onChangeAssetValue}
                            disabled={!this.props.disabledCalculation}
                             />
                    </div>

                    <div className="form-group col-md-4 col-xs-12">
                        <Input 
                            type="select"
                            className="selector"
                            onChange={this.props.onChangeAssetAge}
                            defaultValue='-1'
                            disabled={!this.props.disabledCalculation}
                            >
                            <option value={-1}>Car Age</option>
                           {
                               assetAge.map((age) => {
                                   return <option value={age} key={age}>{age}</option>
                               })
                           }
                        </Input>
                    </div>
                    {
                        loadingPlansRequest && <div className="col-md-1 spinnerLoanAsset">
                            <i className="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i>
                        </div>
                    }
                    {/*<div className="form-group col-md-3">
                        <Input type="text" className="form-control" id="inputCity"/>
                    </div>*/}
                </div>
            </div>
        );
    }

}

export default LoanAssetCalculation